<?php

/**
 * KL_LanguageForums_ControllerAdmin_Forum
 *
 *	@author: Katsulynx
 *  @last_edit:	06.10.2015
 */

class KL_LanguageForums_ControllerAdmin_Forum extends XFCP_KL_LanguageForums_ControllerAdmin_Forum {
    public function actionEdit() {
        $return = parent::actionEdit();
        $return->params['languages'] = $this->_getLanguageModel()->getAllLanguages();
       
        if(isset($return->params['forum']['kl_lang_data'])) {
            $return->params['forum']['kl_lang_data'] = json_decode($return->params['forum']['kl_lang_data']);
            
            if(!is_array($return->params['forum']['kl_lang_data'])) {
                $return->params['forum']['kl_lang_data'] = array(0);   
            }
        }
        else {
            $return->params['forum']['kl_lang_data'] = array(0);   
        }
        
        return $return;
    }
    
    public function actionSave() {
        $return = parent::actionSave();
        
        $writer = XenForo_DataWriter::create('XenForo_DataWriter_Node');
        $nodeId = $this->_input->filterSingle('node_id', XenForo_Input::UINT);
        if(!$nodeId) {
            $nodeName = $this->_input->filterSingle('node_name', XenForo_Input::STRING);
            $node = $this->_getONodeModel()->getNewestNode();
            $writer->setExistingData($node);
        }
        else {
            $writer->setExistingData(array('node_id' => $nodeId));
        }
        
        $languageData = $this->_input->filterSingle('kl_lang_data', XenForo_Input::ARRAY_SIMPLE);
        $writer->set('kl_lang_data',json_encode($languageData));
        $writer->save();
        
        return $return;
    }

	/**
	 * @return XenForo_Model_Node
	 */
	protected function _getONodeModel()
	{
		return $this->getModelFromCache('KL_LanguageForums_Model_Node');
	}

	/**
	 * @return XenForo_Model_Language
	 */
	protected function _getLanguageModel()
	{
		return $this->getModelFromCache('XenForo_Model_Language');
	}
    
    
}