<?php

/**
 * KL_EditorPostTemplates_Model_Editor
 *
 * @author: Nerian
 * @last_edit:    30.08.2015
 */
class KL_LanguageForums_Model_Node extends XenForo_Model {
    public function getNewestNode() {
        return $this->_getDb()->fetchRow('
            SELECT *
            FROM xf_node
            ORDER BY node_id DESC
            LIMIT 1
        ');
    }
}