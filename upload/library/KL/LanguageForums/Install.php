<?php

/**
 * KL_LanguageForums_install
 *
 *	@author: Katsulynx
 *  @last_edit:	06.11.2015
 */

class KL_LanguageForums_Install {
	
    /*
     * TYPE: VARIABLE
     * PURPOSE: ADDON TABLES
     * @last_edit: 06.11.2015
     */
	protected static $_tables = array(
        'kl_lang' => array(
            'install' => "
                ALTER TABLE `xf_node` ADD `kl_lang_data` BLOB NULL;
            ",
            'uninstall' => "
                ALTER TABLE `xf_node` DROP `kl_lang_data`;
            "
        ),
        'kl_user' => array(
            'install' => "
                ALTER TABLE `xf_user` ADD `kl_lang_data` BLOB NULL;
            ",
            'uninstall' => "
                ALTER TABLE `xf_user` DROP `kl_lang_data`
            "
        )
    );
    
    /*
     * TYPE: INSTALL
     * PURPOSE: INSTALL/UPGRADE ADDON
     * @last_edit: 06.11.2015
     */
    public static function install() {
        /* gather required stuff */
        $addonModel = XenForo_Model::create("XenForo_Model_AddOn");
        $dw         = XenForo_DataWriter::create('XenForo_DataWriter_AddOn');
        $db         = XenForo_Application::get('db');
        
        $existingAddon = $addonModel->getAddonById('kl_languageforums');
        
        /* Do a fresh installation */
        if(!$existingAddon) {
            foreach(self::$_tables as $table)
            $db->query($table['install']);
        }
        
        if($existingAddon['version_id'] < 3) {
            $db->query('ALTER TABLE `xf_node` CHANGE `kl_lang_data` `kl_lang_data` BLOB NULL;');
            $db->query('ALTER TABLE `xf_user` CHANGE `kl_lang_data` `kl_lang_data` BLOB NULL;');
        }
    }

    public static function uninstall() {
        $db = XenForo_Application::get('db');
        foreach (self::$_tables as $table) {
            $db->query($table['uninstall']);
        }
    }
}