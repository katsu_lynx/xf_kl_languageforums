<?php

/**
 * KL_LanguageForums_Listener_User
 *
 *	@author: Katsulynx
 *  @last_edit:	19.10.2015
 */

class KL_LanguageForums_Listener_User {
    public static function extendDataWriter($class, array &$extend) {
        $extend[] = 'KL_LanguageForums_DataWriter_User';
    }
    
    public static function extendControllerPublic($class, array &$extend) {
        $extend[] = 'KL_LanguageForums_ControllerPublic_Account';
    }
    
    public static function extendControllerAdmin($class, array &$extend) {
        $extend[] = 'KL_LanguageForums_ControllerAdmin_User';
    }
    
    public static function extendModel($class, array &$extend) {
        $extend[] = 'KL_LanguageForums_Model_User';
    }
}