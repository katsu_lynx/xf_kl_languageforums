<?php

/**
 * KL_LanguageForums_Listener_DataWriter
 *
 *	@author: Katsulynx
 *  @last_edit:	06.10.2015
 */

class KL_LanguageForums_Listener_DataWriter {
    public static function extend($class, array &$extend) {
        $extend[] = 'KL_LanguageForums_DataWriter_Node';
    }
}