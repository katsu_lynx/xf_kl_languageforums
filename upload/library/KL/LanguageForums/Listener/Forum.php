<?php

/**
 * KL_LanguageForums_Listener_Node
 *
 *	@author: Katsulynx
 *  @last_edit:	11.10.2015
 */

class KL_LanguageForums_Listener_Forum {
    public static function extend($class, array &$extend) {
        $extend[] = 'KL_LanguageForums_ControllerPublic_Forum';
    }
}