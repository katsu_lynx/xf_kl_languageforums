<?php

/**
 * KL_LanguageForums_Listener_Node
 *
 *	@author: Katsulynx
 *  @last_edit:	06.10.2015
 */

class KL_LanguageForums_Listener_Node {
    public static function extendPage($class, array &$extend) {
        $extend[] = 'KL_LanguageForums_ControllerAdmin_Page';
    }
    public static function extendCategory($class, array &$extend) {
        $extend[] = 'KL_LanguageForums_ControllerAdmin_Category';
    }
    public static function extendForum($class, array &$extend) {
        $extend[] = 'KL_LanguageForums_ControllerAdmin_Forum';
    }
    public static function extendLinkForum($class, array &$extend) {
        $extend[] = 'KL_LanguageForums_ControllerAdmin_LinkForum';
    }
}