<?php

/**
 * KL_LanguageForums_DataWriter_Node
 *
 *	@author: Katsulynx
 *  @last_edit:	06.10.2015
 */

class KL_LanguageForums_DataWriter_Node extends XFCP_KL_LanguageForums_DataWriter_Node {
    protected function _getFields() {
        $fields = parent::_getFields();
        $fields['xf_node']['kl_lang_data'] = array('type' => self::TYPE_STRING, 'default' => '[0]');
        return $fields;
    }
}