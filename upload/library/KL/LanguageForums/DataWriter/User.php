<?php

/**
 * KL_LanguageForums_DataWriter_User
 *
 *	@author: Katsulynx
 *  @last_edit:	19.10.2015
 */

class KL_LanguageForums_DataWriter_User extends XFCP_KL_LanguageForums_DataWriter_User {
    protected function _getFields() {
        $fields = parent::_getFields();
        $fields['xf_user']['kl_lang_data'] = array('type' => self::TYPE_STRING, 'default' => '[0]');
        return $fields;
    }
}