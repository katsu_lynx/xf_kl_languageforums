<?php

/**
 * KL_LanguageForums_ControllerPublic_Account
 *
 *	@author: Katsulynx
 *  @last_edit:	19.10.2015
 */

class KL_LanguageForums_ControllerPublic_Account extends XFCP_KL_LanguageForums_ControllerPublic_Account {
    public function actionPreferences() {
        $parent = parent::actionPreferences();
        
        $langModel = $this->_getLanguageModel();
        $languages = $langModel->getAllLanguages();
        
        foreach($languages as &$language) {
            unset($language['phrase_cache']);
        }
       
        $parent->subView->params['kl_user_langs'] = json_decode(XenForo_Visitor::getInstance()->kl_lang_data);
        $parent->subView->params['kl_languages'] = $languages;
        
        return $parent;
    }
    
    public function actionPreferencesSave() {
        $parent = parent::actionPreferencesSave();
        
		$this->_assertPostOnly();
        $input = $this->_input->filter(array(
            'kl_lang_data' => XenForo_Input::ARRAY_SIMPLE
        ));
        
        $input['kl_lang_data'] = json_encode($input['kl_lang_data']);
        
        $writer = XenForo_DataWriter::create('XenForo_DataWriter_User');
		$writer->setExistingData(XenForo_Visitor::getUserId());
        $writer->bulkSet($input);
		$writer->preSave();

		if ($dwErrors = $writer->getErrors())
		{
			return $this->responseError($dwErrors);
		}

		$writer->save();
        
        return $parent;
    }
    
    /**
	 * @return XenForo_Model_Language
	 */
	protected function _getLanguageModel()
	{
		return $this->getModelFromCache('XenForo_Model_Language');
	}
}