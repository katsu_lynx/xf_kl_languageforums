<?php

/**
 * KL_LanguageForums_ControllerPublic_Forum
 *
 *	@author: Katsulynx
 *  @last_edit:	19.10.2015
 */

class KL_LanguageForums_ControllerPublic_Forum extends XFCP_KL_LanguageForums_ControllerPublic_Forum {
    
    public function actionIndex() {
        $parent = parent::actionIndex();
        
        $visitor = XenForo_Visitor::getInstance()->toArray();
        $visitor['kl_lang_data'] = json_decode($visitor['kl_lang_data']);
        if(!is_array($visitor['kl_lang_data'])) $visitor['kl_lang_data'] = array();
        $chosenLang = explode(',',$this->_input->filterSingle('lang', XenForo_Input::STRING));
        
        if(!in_array(0, $visitor['kl_lang_data'])) {
            foreach($parent->params['nodeList']['nodesGrouped'] as &$nodeGroup) {
                foreach($nodeGroup as $key => $node) {
                    $node['kl_lang_data'] = json_decode($node['kl_lang_data']);
                    if(is_array($node['kl_lang_data'])) {
                        if(!in_array($visitor['language_id'], $node['kl_lang_data'])
                           && !in_array(0, $node['kl_lang_data'])
                           && empty(array_intersect($visitor['kl_lang_data'], $node['kl_lang_data']))
                           && empty(array_intersect($chosenLang, $node['kl_lang_data']))) {
                            unset($nodeGroup[$key]);
                        }
                    }
                }
            }
        }
        
        $options = XenForo_Application::get('options');
        $langModel = $this->_getLanguageModel();
        $languages = $langModel->getAllLanguages();
        
        shuffle($languages);
        $languages = array_slice(
            $languages,
            0,
            $options->kl_languageforums_numoflangs
        );
        
        foreach($languages as &$language) {
            unset($language['phrase_cache']);
        }
            
        $parent->params['kl_langs'] = $languages;
        
        return $parent;
    }
    
    /**
	 * @return XenForo_Model_Language
	 */
	protected function _getLanguageModel()
	{
		return $this->getModelFromCache('XenForo_Model_Language');
	}
}