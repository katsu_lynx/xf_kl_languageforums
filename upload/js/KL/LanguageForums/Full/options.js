/**
 * KL_LanguageForums_options
 *
 *	@author: Katsulynx
 *  @last_edit:	19.10.2015
 */
function filter() {
    var language = $('#ctrl_language_id').val();
    $('#kl_lang_data').children('option').show();
    $('#kl_langdat_'+language).hide();
}

$(document).ready(function() {
    filter(); 
    $('#ctrl_language_id').on('change',function(){filter()});
});